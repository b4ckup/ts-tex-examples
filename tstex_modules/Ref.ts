import { IModule, ICompletionItemProvider, IResolver, IRootNode, ISourceFile, ICompletionItem, IFnNode } from "./_api";

export type RefKind = "tab" | "sec" | "subsec" | "subsubsec" | "chap" | "fig" | "other"; //This is just an example to show the usefullness of typesafety

export class Ref implements IModule
{
  resolver: IResolver;
  provider: RefCompletionProvider;

  _init(resolver: IResolver) //this is called when the module is instantiated (on every save of this .ts file)
  {
    this.resolver = resolver;
    resolver.register(this, "Ref");
  }

  _prebuild()
  {
    if(this.provider == null)
      this.provider = this.resolver.resolve<RefCompletionProvider>("RefCompletionProvider");
    else
      console.log("Could not resolve provider");
  }

  label(name: string, kind: RefKind = "other")
  {
    let label = kind+":"+name
    if(this.provider != null)
      this.provider.suggestions.push(label);
    return `\\label{${label}}`;
  }

  ref(name: string)
  {
    return `\\autoref{${name}}`;
  }

  _finalize()
  {
    this.resolver.unregister(this);
  }
}

export class RefCompletionProvider implements ICompletionItemProvider
{
  resolver: IResolver;
  suggestions = [];
  _init(resolver: IResolver) //this is called when the module is instantiated (on every save of this .ts file)
  {
    this.resolver = resolver;
    resolver.register(this, "RefCompletionProvider"); //Register this completion provider to make it available for other modules
  }
  
  _prebuild()
  {
    this.suggestions = [];
  }

  _finalize() //this is called when the module is unloaded (on every save of this .ts file)
  {
    this.resolver.unregister(this);
  }

  //Provide already registered labels as completion to the ref() function. Note that completions are only added to suggestions on node evaluation therefore you have to build once for anything to be in suggestions
  provideCompletionItems(triggerChar: string, node: IRootNode, sf: ISourceFile): ICompletionItem[]
  {
    if(triggerChar != "\"")
      return null;
    if(node.type == "fn")
    {
      let n = node as IFnNode;
      if(n.name.name == "this.ref.ref")
      {
        return this.suggestions
          .filter((s, i, self) => {
            return self.indexOf(s) === i;
          })
          .map(s => {
            return  {
              insertText: s,
              name: s,
              sortText: s
            }
          });
      }
    }
    return null;
  }
}
