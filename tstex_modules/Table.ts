import { IModule, IResolver } from './_api';
import { Ref } from './Ref';


export class Table implements IModule
{
  resolver: IResolver
  ref: Ref
  _init(resolver: IResolver)
  {
    this.resolver = resolver;
  }

  _prebuild()
  {
    this.ref = this.resolver.resolve("Ref");
  }

  //Function to create a tabular
  *table(data: any[][], label: string, columnFormat: string = null, cellConverter: (cell: any) => any)
  {
    if(data.length == 0)
      return "";

    yield "\\begin{tabular}{"
    if(columnFormat == null)
      columnFormat = "|" + data[0].map(d => "c").join("|") + "|"
    
    yield columnFormat + "}"
    if(this.ref != null)
      yield this.ref.label(label, "tab");
    yield "\\hline";
    yield data.map(row => {
      return row.map(c => {
        if(cellConverter != null)
          c = cellConverter(c);
        return c;
      }).join(" & ") + "\\\\ \\hline"; //remember to escape \\
    });
    yield "\\end{tabular}";
  }
}